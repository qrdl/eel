/**************************************************************************
 *
 *  File:       test.c
 *
 *  Project:    Eel (https://gitlab.com/qrdl/eel)
 *
 *  Descr:      Error handling and logging
 *
 *  Comments:   Test and sample
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "eel.h"

FILE *logfd;

#define FOO() foo(__FILE__, __LINE__)

int foo(char *file, int line) {
    int ret = 0;
    LOCAL_LOG('I', file, line, "From foo(), but reported line within main()");
    RETCLEAN(1);

cleanup:
    return ret;
}

int main(void) {
    int ret = EXIT_SUCCESS;
    logfd = stderr;

    ERR("Error %s", "text");
    WARN("Warning %d", 42);
    INFO("Info %.2f", 3.141593);
    DBG("Debug %s %d", "bar", 7);

    if (FOO())
        RETCLEAN(EXIT_FAILURE);

cleanup:

    return ret;
}

