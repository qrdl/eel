# Eel
Eel is simple error-handling and logging library.

## Features

#### Logging
- supports different logging levels - error, warning, info and debug (only
printed if compiled with DEBUG defined)
- prints filename and line in log string
- supports printf-like format
- allows to print log from function but report different location (useful for
reporting errors for caller)
- specify logging file by defining and assigning `logfd` variable (setting
`logfd` to `NULL` disables logging)

#### Error handling
Eel is based on the concept that all functions should have single exit point,
in order to prevent possible resource leaks, and using `goto` for error
handling is justified, and results in cleaner code.

## Compiling
Eel is a single-header library, so no compiling is required. Enclosed Makefile
is needed only for building test, just run `make` command

## Use
In order to use Eel you need to include eel.h into your source file.

#### Logging
To use logging you need to define and assing logfd variable, like this:
`FILE *logfd = stderr`. Then you just use `ERR()`, `WARN()`, `INFO()` and
`DBG()` macros to print your log messages. All these macros take variable
number of parameters - printf-like format, and optional parameters that
correspond with format. Modern compilers can check if parameters match
the format, it is recommended to use this checking in order to avoid
undefined behaviour. GCC and clang use `-Wformat` option for this.
If source is compiled with defined DEBUG macro, DBG messages will be printed,
otherwise all debug output will be suppressed.

#### Error handling
To use `RETCLEAN()` macro within the function there should be `ret` variable
defined, and `cleanup` label, where `RETCLEAN()` jumps to.
See function `foo()` in file test.c 

## License
This project is licensed under the MIT License - see the [license](LICENSE) for
details

## Author(s)
- Ilya Caramishev - https://gitlab.com/qrdl

