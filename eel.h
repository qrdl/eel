/**************************************************************************
 *
 *  File:       eel.h
 *
 *  Project:    Eel (https://gitlab.com/qrdl/eel)
 *
 *  Descr:      Error handling and logging
 *
 *  Comments:   Single-header logging and error handling library
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef EEL_H
#define EEL_H

#include <stdio.h>
#include <string.h>

/* use of RETCLEAN() requires local ret variable, and cleanup label */
#define RETCLEAN(A) do { ret = A; goto cleanup; } while(0)

extern FILE *logfd;

/* POSIX basename() may modify the perameter, therefore it cannot be called
 * for string literal, so it is safer to roll my own */
#define LOCAL_LOG(T, F, L, ...) do { \
        if (!logfd) break; \
        char *tmp = strrchr((F), '/'); \
        fprintf(logfd, "%c:%s:%d:", (T), tmp ? tmp + 1 : (F), (L)); \
        fprintf(logfd, __VA_ARGS__); \
        fprintf(logfd, "\n"); \
	fflush(logfd); \
    } while(0)

#define LOG(T, ...) LOCAL_LOG(T, __FILE__, __LINE__, __VA_ARGS__)

#define ERR(...)    LOG('E', __VA_ARGS__)
#define WARN(...)   LOG('W', __VA_ARGS__)
#define INFO(...)   LOG('I', __VA_ARGS__)
/* DEBUG messages are only printed if DEBUG defined */
#ifdef DEBUG
#define DBG(...)    LOG('D', __VA_ARGS__)
#else
#define DBG(...)     
#endif

#endif
